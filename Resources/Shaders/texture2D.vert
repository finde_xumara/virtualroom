#version 100

uniform mat4	uMVP;
uniform vec3	uPosition;

attribute vec3 aVertex;
attribute vec2	aTexCoord0;

varying vec3 vGrid;
varying highp vec2	TexCoord0;

void main( void )
{
    vGrid = aVertex + uPosition;
    vec4 pos = vec4(vGrid, 1.0);
    
    TexCoord0 	= aTexCoord0;
    gl_Position	= uMVP * pos;
}
