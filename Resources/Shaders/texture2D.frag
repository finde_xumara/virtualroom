#version 100

#ifdef GL_ES
    precision highp float;
#endif

uniform sampler2D 		uTex0;

varying highp vec2		TexCoord0;

void main( void )
{
    vec3 color = texture2D( uTex0, TexCoord0 ).rgb;
    gl_FragColor = vec4( color, 1.0 );
}