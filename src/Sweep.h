//
//  Sweep.h
//  VirtualRoom
//
//  Created by Finde Xumara on 03/05/16.
//
//

#import <Mantle/Mantle.h>
#import <GLKit/GLKit.h>

@interface Sweep : MTLModel<MTLJSONSerializing>

@property (nonatomic, strong) NSString *scanID;
@property (nonatomic, strong) NSString *sweepID;
@property (nonatomic, strong) NSValue *positionValue;
@property (nonatomic, strong) NSValue *quaternionValue;
@property (nonatomic) bool isVisible;
@property (nonatomic) bool isFocused;

- (GLKVector3) position;
- (GLKQuaternion) quaternion;

@end