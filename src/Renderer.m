#if !defined(__has_feature) || !__has_feature(objc_arc)
#error "This file requires ARC support. Compile with -fobjc-arc"
#endif

#define NUM_CUBE_VERTICES 108
#define NUM_CUBE_COLORS 144
#define NUM_GRID_VERTICES 72
#define NUM_GRID_COLORS 96
#define MAX_SHADER_LENGTH 8192

#define MOVE_DELAY 3
#define NUM_CIRCLE_VERTICES 720

#import "PreCalc_math.h"
#import "Renderer.h"

#import <AudioToolbox/AudioToolbox.h>
#import <GLKit/GLKit.h>
#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>
#import <QuartzCore/QuartzCore.h>

#import "GCSCardboardAudioEngine.h"
#import "GCSHeadTransform.h"

#import "Sweep.h"
#import "Hand.h"

// Cube focus angle threshold in radians.
static const float kFocusThresholdRadians = 0.5f;

static GLuint LoadShader(GLenum type, const char *shader_src) {
    GLint compiled = 0;
    
    // Create the shader object
    const GLuint shader = glCreateShader(type);
    if (shader == 0) {
        return 0;
    }
    // Load the shader source
    glShaderSource(shader, 1, &shader_src, NULL);
    
    // Compile the shader
    glCompileShader(shader);
    // Check the compile status
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
    
    if (!compiled) {
        GLint info_len = 0;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &info_len);
        
        if (info_len > 1) {
            char *info_log = ((char *)malloc(sizeof(char) * info_len));
            glGetShaderInfoLog(shader, info_len, NULL, info_log);
            NSLog(@"Error compiling shader:%s", info_log);
            free(info_log);
        }
        glDeleteShader(shader);
        return 0;
    }
    return shader;
}

// Checks the link status of the given program.
static bool checkProgramLinkStatus(GLuint shader_program) {
    GLint linked = 0;
    glGetProgramiv(shader_program, GL_LINK_STATUS, &linked);
    
    if (!linked) {
        GLint info_len = 0;
        glGetProgramiv(shader_program, GL_INFO_LOG_LENGTH, &info_len);
        
        if (info_len > 1) {
            char *info_log = ((char *)malloc(sizeof(char) * info_len));
            glGetProgramInfoLog(shader_program, info_len, NULL, info_log);
            NSLog(@"Error linking program: %s", info_log);
            free(info_log);
        }
        glDeleteProgram(shader_program);
        return false;
    }
    return true;
}

@implementation Renderer {
    
    NSMutableArray *_skyboxCubemap;
    GLKSkyboxEffect *_skyboxEffect;
    GLKBaseEffect *_circleEffect;
    
    GLuint _circle_vertex_array;
    GLuint _circle_vertex_buffer;
    GLuint _circle_index_buffer;
    
    NSMutableArray *_sweeps;
    
    bool _is_init_set;
    Sweep *_current_sweep;
    
    bool _has_previous_focus;
    NSDate *startMarker;
    Sweep *_next_sweep;
    
    GLKVector4 _basic_color;
    GLKVector4 _found_color;
    NSUInteger _index;
}

#pragma mark - GCSCardboardViewDelegate overrides

- (const char *) LoadFile:(nullable NSString *) filename
                   ofType:(nullable NSString *) extension {
    
    NSString * dbFile = [[NSBundle mainBundle] pathForResource:filename ofType:extension];
    const char * contents = [[NSString stringWithContentsOfFile:dbFile
                                                       encoding:NSASCIIStringEncoding error:nil] cStringUsingEncoding:NSASCIIStringEncoding];
    return contents;
}

- (void)cardboardView:(GCSCardboardView *)cardboardView
     willStartDrawing:(GCSHeadTransform *)headTransform {
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"Data/3du_2/sweeps" ofType:@"json"];
    
    //    NSLog(@"%@", jsonString);
    NSError *error = nil;
    NSData *JSONData = [NSData dataWithContentsOfFile:filePath
                                              options:NSDataReadingMappedIfSafe
                                                error:&error];
    
    // Now create a NSDictionary from the JSON data
    NSDictionary *JSONObject = [NSJSONSerialization
                                JSONObjectWithData:JSONData
                                options:0
                                error:&error];
    
    _sweeps = [[NSMutableArray alloc] init];
    
    // Load all sweeps
    for(NSDictionary *dict in [JSONObject objectForKey:@"Sweeps"]) {
        
        // Create a new Sweep object for each one and initialise it with information in the dictionary
        Sweep *sweep = [MTLJSONAdapter modelOfClass: Sweep.class
                                 fromJSONDictionary: dict
                                              error: &error];
        
        sweep.isVisible = NO;
        sweep.isFocused = NO;
        
        // Add the Location object to the array
        [_sweeps addObject:sweep];
        
    }
    
    _index = 0;
    _current_sweep = [_sweeps objectAtIndex:_index];
    
    //////////////////
    //
    //  Load skybox
    _skyboxCubemap = [[NSMutableArray alloc] init];
    for (Sweep *sweep in _sweeps) {
        NSString *_dir = @"Data/%@/%@/%d";
        NSString *_scan_id = sweep.scanID;
        NSString *_sweep_id = sweep.sweepID;
        NSLog(@"%@", [NSString stringWithFormat:_dir, _scan_id, _sweep_id, 1]);
        NSArray *skyboxCubeMapFilenames = @[
                                            [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:_dir, _scan_id, _sweep_id, 1] ofType:@"jpg"], // right
                                            [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:_dir, _scan_id, _sweep_id, 3] ofType:@"jpg"], // left
                                            [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:_dir, _scan_id, _sweep_id, 4] ofType:@"jpg"], // top
                                            [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:_dir, _scan_id, _sweep_id, 5] ofType:@"jpg"], // bottom
                                            [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:_dir, _scan_id, _sweep_id, 0] ofType:@"jpg"], // front
                                            [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:_dir, _scan_id, _sweep_id, 2] ofType:@"jpg"], // back
                                            ];
        NSDictionary *options = @{ GLKTextureLoaderOriginBottomLeft: [NSNumber numberWithBool:NO] };
        [_skyboxCubemap addObject: [GLKTextureLoader cubeMapWithContentsOfFiles:skyboxCubeMapFilenames
                                                                        options:options
                                                                          error:&error]];
    }
    
    _skyboxEffect = [[GLKSkyboxEffect alloc] init];
    _skyboxEffect.label = @"Main Sky Box Effect";
    
    /////////////////
    //
    //  Load Circle
    
    _found_color = GLKVector4Make(245.0f/255.0f, 130.0f/255.0f, 32.0f/255.0f, 0.3f);
    _basic_color = GLKVector4Make(1,1, 1, 0.3f);
    
    _circleEffect = [[GLKBaseEffect alloc] init];
//    _circleEffect.constantColor = _basic_color;
//    _circleEffect.useConstantColor = NO;
    _circleEffect.light0.diffuseColor = _basic_color;
    _circleEffect.light0.enabled = GL_TRUE;
    _circleEffect.light0.position = GLKVector4Make(0,0,0,1);
    
    glEnable(GL_DEPTH_TEST);
    
    glGenVertexArraysOES(1, &_circle_vertex_array);
    glBindVertexArrayOES(_circle_vertex_array);
    
    glGenBuffers(1, &_circle_vertex_buffer);
    glBindBuffer(GL_ARRAY_BUFFER, _circle_vertex_buffer);
    glBufferData(GL_ARRAY_BUFFER, Hand_IVAlength*sizeof(GL_FLOAT), Hand_IVA, GL_STATIC_DRAW);
    
    GLsizei stride = 8*sizeof(GLfloat);
    
    glEnableVertexAttribArray(GLKVertexAttribPosition);
    glVertexAttribPointer(GLKVertexAttribPosition, 3, GL_FLOAT, GL_FALSE, stride, (char *)(0));
    glEnableVertexAttribArray(GLKVertexAttribNormal);
    glVertexAttribPointer(GLKVertexAttribNormal, 3, GL_FLOAT, GL_FALSE, stride, (char *)(3*sizeof(GLfloat)));
    
    // Note: we don't use the texture coords in this demo; ideally they would be stripped out of the vertex data.
    
    glGenBuffers(1, &_circle_index_buffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _circle_index_buffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, Hand_NumIndices*sizeof(GLushort), Hand_Indices, GL_STATIC_DRAW);
    
    glBindVertexArrayOES(0);
}

// -update
- (void)cardboardView:(GCSCardboardView *)cardboardView
     prepareDrawFrame:(GCSHeadTransform *)headTransform {
    
    GLKTextureInfo *cubemapTexture = [_skyboxCubemap objectAtIndex: _index];
    if (_skyboxEffect.textureCubeMap.name != cubemapTexture.name)
        _skyboxEffect.textureCubeMap.name = cubemapTexture.name;
    
    // Update audio listener's head rotation.
    const GLKQuaternion head_rotation = GLKQuaternionMakeWithMatrix4(GLKMatrix4Transpose([headTransform headPoseInStartSpace]));
    
    /// for every sweep
    bool has_focus = NO;
    for (Sweep *sweep in _sweeps) {
        
        // check if in distance
        
        GLKVector3 circle_pos_relative = [self getRelativePosition:sweep from:_current_sweep];
        CGPoint cg_pos_relative = CGPointMake(circle_pos_relative.x, circle_pos_relative.z);

        // if within distance
        float distance_squared = DISTSQ(cg_pos_relative,VEC0);
        sweep.isVisible = (distance_squared <= powf(3.0, 2) && _current_sweep.sweepID!=sweep.sweepID);

        // check circle is focused
        if (sweep.isVisible) {
            sweep.isFocused = [self isLookingAtObject:&head_rotation sourcePosition:&circle_pos_relative];
            
            if (sweep.isFocused) {
                _next_sweep = sweep;
                has_focus = YES;
//
                // if stare long enough
                if (fabs([startMarker timeIntervalSinceNow]) > MOVE_DELAY) {
                    sweep.isFocused = NO;
                    has_focus = NO;
                    _current_sweep = sweep;
                    _index = [_sweeps indexOfObject:_current_sweep];
                    startMarker = [NSDate date];
                }

            }
        } else {
            sweep.isFocused = NO;
        }
    }
    
    if (!has_focus) {
        _next_sweep = _current_sweep;
        startMarker = [NSDate date];
        
    } else {
        
        if (!_has_previous_focus) {
            startMarker = [NSDate date];
        }
    }
    
    _has_previous_focus = has_focus;
    
    // Clear GL viewport.
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glEnable(GL_DEPTH_TEST);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_SCISSOR_TEST);
}

- (void)cardboardView:(GCSCardboardView *)cardboardView
              drawEye:(GCSEye)eye
    withHeadTransform:(GCSHeadTransform *)headTransform {
    CGRect viewport = [headTransform viewportForEye:eye];
    glViewport(viewport.origin.x, viewport.origin.y, viewport.size.width, viewport.size.height);
    glScissor(viewport.origin.x, viewport.origin.y, viewport.size.width, viewport.size.height);
    
    // Get the head matrix.
    const GLKMatrix4 head_from_start_matrix = [headTransform headPoseInStartSpace];
    
    // Get this eye's matrices.
    GLKMatrix4 projection_matrix = [headTransform projectionMatrixForEye:eye near:0.1f far:100.0f];
    GLKMatrix4 eye_from_head_matrix = [headTransform eyeFromHeadMatrix:eye];
    
    // Compute the model view projection matrix.
    GLKMatrix4 model_view_projection_matrix = GLKMatrix4Multiply(
                                                                 projection_matrix, GLKMatrix4Multiply(eye_from_head_matrix, head_from_start_matrix));
    
    // Render from this eye.
    //    [self renderWithModelViewProjectionMatrix:model_view_projection_matrix.m];
    [self renderWithModelViewProjectionMatrix:model_view_projection_matrix];
}

- (GLKVector3) getRelativePosition:(Sweep *) sweep
                              from:(Sweep *) anchor_sweep {
    
    if ( sweep.sweepID == anchor_sweep.sweepID) {
        return GLKVector3Make(0, 0, 0);
    }
    
    GLKVector3 relPos = GLKVector3Subtract(sweep.position, anchor_sweep.position);
    
    return relPos;
}

- (GLKQuaternion) getGlobalQuaternion:(Sweep *) sweep {
    
    return sweep.quaternion;
    
    NSUInteger current_index = [_sweeps indexOfObject:sweep];
    
    if ( current_index == 0) {
        return GLKQuaternionIdentity;
    } else {
        GLKQuaternion _prev_global_quat = [self getGlobalQuaternion:[_sweeps objectAtIndex:current_index-1]];
        
        return GLKQuaternionMultiply(_prev_global_quat, sweep.quaternion);
    }
}

#pragma Draw Model
// -draw
- (void)renderWithModelViewProjectionMatrix:(GLKMatrix4) model_view_matrix {
    // draw cubemap
    
    // TODO:: calibrate the cubemap using quaternion
    GLKMatrix4 cubeViewMatrix = GLKMatrix4Identity;
    GLKQuaternion _rel_quat = [self getGlobalQuaternion: _current_sweep];
    
    GLKVector3 axis = GLKQuaternionAxis(_rel_quat);
    float angle = GLKQuaternionAngle(_rel_quat);
    cubeViewMatrix = GLKMatrix4Multiply(cubeViewMatrix, model_view_matrix);
    
    if (angle > 0) {
        cubeViewMatrix = GLKMatrix4Rotate(cubeViewMatrix, angle, axis.x, axis.y, axis.z);
    }
    
    _skyboxEffect.transform.modelviewMatrix = cubeViewMatrix;
    
    _skyboxEffect.xSize = -5;
    _skyboxEffect.ySize = 5;
    _skyboxEffect.zSize = 5;
    
    [_skyboxEffect prepareToDraw];
    [_skyboxEffect draw];
    
    
    // draw circle
    for (Sweep *sweep in _sweeps) {

        if (sweep == _current_sweep)
            continue;
        
        CGPoint _look_init = CGPointMake(0, -1);

        GLKVector3 rel_pos = [self getRelativePosition:sweep from:_current_sweep];
        rel_pos.x *= -1;
        CGPoint cg_pos_relative = VTP(GLKVector2Normalize(GLKVector2Make(rel_pos.x, rel_pos.z)));

        if (sweep.isVisible) {

            GLKMatrix4 modelViewMatrix = model_view_matrix;
            float angle_pos_deg = ANGLEV(PTV(cg_pos_relative), PTV(_look_init));
            
            if (CLOCK(PTV(cg_pos_relative), PTV(_look_init))) {
                angle_pos_deg *= -1;
            }

            float angle_pos = GLKMathDegreesToRadians(angle_pos_deg);
                  
            if( angle_pos != 0.f ) {
                modelViewMatrix = GLKMatrix4Rotate(modelViewMatrix, angle_pos, 0, 1, 0);
            }
            
            modelViewMatrix = GLKMatrix4Translate(modelViewMatrix, 0.0, 0.0, -0.8f - (fabs([startMarker timeIntervalSinceNow]) / MOVE_DELAY * 1.0f ));
            
            modelViewMatrix = GLKMatrix4Scale(modelViewMatrix, 0.2, 0.2, 0.2);

            _circleEffect.transform.modelviewMatrix = modelViewMatrix;
            
            if (sweep.isFocused) {
                _circleEffect.constantColor = _found_color;
                _circleEffect.light0.diffuseColor = _found_color;
            } else {
                _circleEffect.constantColor = _basic_color;
                _circleEffect.light0.diffuseColor = _basic_color;
            }
            
            glBindVertexArrayOES(_circle_vertex_array);
            [_circleEffect prepareToDraw];
            glDrawElements(GL_TRIANGLES, Hand_NumIndices, GL_UNSIGNED_SHORT, NULL);
        }
    }
}

- (void)cardboardView:(GCSCardboardView *)cardboardView
         didFireEvent:(GCSUserEvent)event {
    switch (event) {
        case kGCSUserEventBackButton:
            NSLog(@"User pressed back button");
            break;
        case kGCSUserEventTilt:
            NSLog(@"User performed tilt action");
            break;
        case kGCSUserEventTrigger:
            NSLog(@"User performed trigger action");
            _next_sweep = [_sweeps objectAtIndex:(_index+1)];
            _current_sweep = _next_sweep;
            _index = [_sweeps indexOfObject:_current_sweep];
            break;
    }
}

- (void)cardboardView:(GCSCardboardView *)cardboardView shouldPauseDrawing:(BOOL)pause {
    if ([self.delegate respondsToSelector:@selector(shouldPauseRenderLoop:)]) {
        [self.delegate shouldPauseRenderLoop:pause];
    }
}

// Returns whether the object is currently on focus.
- (bool)isLookingAtObject:(const GLKQuaternion *)head_rotation
           sourcePosition:(GLKVector3 *)position {
    GLKVector3 source_direction = GLKQuaternionRotateVector3(
                                                             GLKQuaternionInvert(*head_rotation), *position);
    return ABS(source_direction.v[0]) < kFocusThresholdRadians &&
    ABS(source_direction.v[1]) < kFocusThresholdRadians && -source_direction.z > 0;
}

@end
