#import "ViewController.h"

#import "RenderLoop.h"
#import "Renderer.h"

@interface ViewController ()<RendererDelegate> {
  GCSCardboardView *_cardboardView;
  Renderer *_Renderer;
  RenderLoop *_renderLoop;
}
@end

@implementation ViewController

- (void)loadView {
  _Renderer = [[Renderer alloc] init];
  _Renderer.delegate = self;

  _cardboardView = [[GCSCardboardView alloc] initWithFrame:CGRectZero];
  _cardboardView.delegate = _Renderer;
  _cardboardView.autoresizingMask =
      UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;

  _cardboardView.vrModeEnabled = YES;

  // Use double-tap gesture to toggle between VR and magic window mode.
  UITapGestureRecognizer *doubleTapGesture =
      [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didDoubleTapView:)];
  doubleTapGesture.numberOfTapsRequired = 2;
  [_cardboardView addGestureRecognizer:doubleTapGesture];

  self.view = _cardboardView;
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];

  _renderLoop = [[RenderLoop alloc] initWithRenderTarget:_cardboardView
                                                            selector:@selector(render)];
}

- (void)viewDidDisappear:(BOOL)animated {
  [super viewDidDisappear:animated];

  // Invalidate the render loop so that it removes the strong reference to cardboardView.
  [_renderLoop invalidate];
  _renderLoop = nil;
}

#pragma mark - RendererDelegate

- (void)shouldPauseRenderLoop:(BOOL)pause {
  _renderLoop.paused = pause;
}

#pragma mark - Implementation

- (void)didDoubleTapView:(id)sender {
  _cardboardView.vrModeEnabled = !_cardboardView.vrModeEnabled;
}

@end
