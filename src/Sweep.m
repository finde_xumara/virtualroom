//
//  Sweep.m
//  VirtualRoom
//
//  Created by Finde Xumara on 03/05/16.
//
//

#import "Sweep.h"

@implementation Sweep

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"scanID" : @"scan_id",
             @"sweepID" : @"sweep_id",
             @"positionValue" : @"position",
             @"quaternionValue" : @"quaternion"
             };
}

+ (NSValueTransformer*) positionValueJSONTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSArray<NSNumber*>* t, BOOL *success, NSError *__autoreleasing *error)
            {
                NSParameterAssert(t.count == 3);
                
                float f[3];
                for (int i = 0; i < 3; i++) f[i] = [t[i] floatValue];
                GLKVector3 m = GLKVector3Make(f[0], f[1], f[2]);
                
                NSValue *mValue = [NSValue value: &m withObjCType: @encode(GLKVector3)];
                *success = mValue;
                return mValue;
            }
                                                reverseBlock:^id(NSValue *mValue, BOOL *success, NSError *__autoreleasing *error)
            {
                GLKVector3 m; [mValue getValue: &m];
                
                NSMutableArray<NSNumber*> *t = [NSMutableArray new];
                for (int i = 0; i < 3; i++) [t addObject: @(m.v[i])];
                
                *success = t;
                return t;
            }];
}

+ (NSValueTransformer*) quaternionValueJSONTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSArray<NSNumber*>* t, BOOL *success, NSError *__autoreleasing *error)
            {
                NSParameterAssert(t.count == 4);
                
                float f[4];
                // w, x, y ,z
                for (int i = 0; i < 4; i++) f[i] = [t[i] floatValue];
                GLKQuaternion m = GLKQuaternionMake(f[1], f[2], f[3], f[0]);
                
                NSValue *mValue = [NSValue value: &m withObjCType: @encode(GLKVector4)];
                *success = mValue;
                return mValue;
            }
                                                reverseBlock:^id(NSValue *mValue, BOOL *success, NSError *__autoreleasing *error)
            {
                GLKQuaternion m; [mValue getValue: &m];
                
                NSMutableArray<NSNumber*> *t = [NSMutableArray new];
                [t addObject: @(m.w)];
                [t addObject: @(m.x)];
                [t addObject: @(m.y)];
                [t addObject: @(m.z)];
                
                *success = t;
                return t;
            }];
}

- (GLKVector3) position
{
    GLKVector3 m; [self.positionValue getValue: &m];
    return m;
}

- (GLKQuaternion) quaternion
{
    GLKQuaternion m; [self.quaternionValue getValue: &m];
    return m;
}

@end
