#import "GCSCardboardView.h"

/** App renderer delegate. */
@protocol RendererDelegate <NSObject>
@optional

/** Called to pause the render loop because a 2D UI is overlaid on top of the renderer. */
- (void)shouldPauseRenderLoop:(BOOL)pause;

@end

/** TreasureHunt renderer. */
@interface Renderer : NSObject<GCSCardboardViewDelegate>

@property(nonatomic, weak) id<RendererDelegate> delegate;

@end

